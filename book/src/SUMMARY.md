<!--
SPDX-FileCopyrightText: 2022 Heiko Schaefer <heiko@schaefer.name>
SPDX-License-Identifier: MIT OR Apache-2.0
-->

# Summary

- [OpenPGP cards](context.md)
- [Setting up cards with `opgpcard`](opgpcard.md)
- [SSH authentication](ssh.md)
- [git commit signing](git.md)
- [Thunderbird](thunderbird.md)
