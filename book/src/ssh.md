<!--
SPDX-FileCopyrightText: 2022 Heiko Schaefer <heiko@schaefer.name>
SPDX-License-Identifier: MIT OR Apache-2.0
-->

# SSH with OpenPGP cards TL;DR

Install and start service:

```
$ cargo install openpgp-card-ssh-agent  # installs to $HOME/.cargo/bin/
$ openpgp-card-ssh-agent -H unix://$XDG_RUNTIME_DIR/ocsa.sock
```

Set up a card for use (not persistent between runs of openpgp-card-ssh-agent):

```
$ export SSH_AUTH_SOCK=$XDG_RUNTIME_DIR/ocsa.sock
$ ssh-add -s FFFE:01234567  # enter User PIN
```

Log into remote machines

```
$ export SSH_AUTH_SOCK=$XDG_RUNTIME_DIR/ocsa.sock
$ ssh <hostname>
```


# SSH login using an OpenPGP card

In this guide, we'll set up OpenPGP card-based SSH logins to a remote machine.

We assume that you have an OpenPGP card plugged into your client machine, and that an authentication key is available
on that card.

We also assume that the `opgpcard` tool [is installed](https://openpgp-card.gitlab.io/guide/opgpcard.html#install).

## Optional: generate throwaway keys on your card, for this guide

If you have a blank OpenPGP card, and want to generate throwaway keys to try this guide, you can generate keys on the
card by running:

```
$ opgpcard admin -c FFFE:01234567 generate -o /tmp/cert.pub 25519
```

Replace `FFFE:01234567` with the identifier of your own card (as shown by `opgpcard list`).

This command instructs your card to generate a set of ECC Curve 25519 keys. Not all cards support this algorithm.
If yours doesn't, you can generate RSA 2048 keys instead:

```
$ opgpcard admin -c FFFE:01234567 generate -o /tmp/cert.pub rsa2048
```

Key generation will ask for both the Admin PIN (`12345678` by default) and the User PIN (`123456` by default).
You can ignore the public key output in `/tmp/cert.pub`, if you don't want to use these keys outside this guide.

If you only generate keys to try this guide, you'll probably want to run `opgpcard factory-reset -c <identifier>` at
the end. This will revert your card back into a blank state (and remove our throwaway keys).

# Allowing access to the remote server

Now we'll add our "SSH public key" to the remote machine's list of authorized keys.

More specifically, we'll add the SSH public key that corresponds to the authentication key on our OpenPGP card.
This tells the remote machine that we want our OpenPGP card to be considered a valid means of authorization to log in.

## Adding the public key to `authorized_keys` on a remote machine

To see the SSH public key representation of the authentication key on our OpenPGP card, we run `opgpcard ssh`:

```
$ opgpcard ssh
OpenPGP card FFFE:01234567

Authentication key fingerprint:
7A21F955A14C1C134D9EAE90E8B3EFD2581F3113

SSH public key:
ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIP3GtBCzGAUjg8iN9JOo5f+cFCYLaWE8titbIbimtKwe opgpcard:FFFE:01234567
```

In this output, the last line shows our SSH public key, which is:

`ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIP3GtBCzGAUjg8iN9JOo5f+cFCYLaWE8titbIbimtKwe opgpcard:FFFE:01234567`

We add this line to the `~/.ssh/authorized_keys` file in an account on a remote server.

Now the SSH daemon on that machine will consider the authentication key on our OpenPGP card as proof that we're allowed
to log in.

(Note that the public key line doesn't need to be kept secret. Only its counterpart, the secret key on our card, is
sensitive.)

# Setting up the client side

Next, we install the experimental all-in-one ssh-agent on our machine (which is coupled with the SSH client by
the [SSH authentication protocol](https://datatracker.ietf.org/doc/html/rfc4252)).

In this guide, for demonstration purposes, we'll run the service in a terminal, as a Rust debug build
(in production systems, you'll probably want to run release builds as system services).

NOTE: This service is in an early experimental stage. Don't use it in sensitive contexts, just yet.

```
$ cargo install openpgp-card-ssh-agent
$ openpgp-card-ssh-agent -H unix://$XDG_RUNTIME_DIR/ocsa.sock
```

## Using the SSH Agent

To use `openpgp-card-ssh-agent` with your system's SSH client, the variable `SSH_AUTH_SOCK` must point to it:

`export SSH_AUTH_SOCK=$XDG_RUNTIME_DIR/ocsa.sock`

### Register a card with the agent

Once the service is running, you can register an OpenPGP card with the `openpgp-card-ssh-agent` OpenSSH agent,
by telling it to add the authentication key on the card:

`ssh-add -s FFFE:01234567`

`FFFE:01234567` is the identifier of our card (as shown in the output of `opgpcard list`).

You will be prompted to "Enter passphrase for PKCS#11" (don't let this message confuse you: you actually need to enter
the User PIN for your OpenPGP card, but the SSH software doesn't know this).

Remember that the User PIN is `123456` by default, on most cards (and remember to change the PIN for production cards).

This operation is not persisting anything on disk, for now. You always need to 'add' keys to `openpgp-card-ssh-agent`
after starting it.

## Log into remote machines

When `SSH_AUTH_SOCK` is set, you should now be able to `ssh <remote-machine>`.
This will use the authentication key on your OpenPGP card to authorize your login.

If your OpenPGP card is configured to require touch confirmation for "authentication" operations,
you will get a desktop notification from the SSH agent that reminds you to confirm the operation.

Yay!
